\chapter{Introduction and Motivation}

\begin{fluff}
	In this chapter, we will introduce various concepts and mathematical terms that will be studied in more detail throughout the rest of the lecture course.
	The overarching theme is the translation between algebra and geometry/topology.
\end{fluff}





\section{Operator Algebras}

\begin{definition}
	A~\defemph{\staralgebra} is a complex algebra together with an algebra-antiinvolution~$(\ph)^*$.
\end{definition}


\begin{definition}
	A~\defemph{\Cstaralgebra{}} is a~\staralgebra{}~$A$ together with a norm~$\norm{\phdot}$ on~$A$ such that
	\begin{enumerate}
		\item
			$A$ is complete with respect to~$\norm{\phdot}$ and
		\item
			$\norm{a^* a} = \norm{a}^2$ for every~$a \in A$.
	\end{enumerate}
	The second condition is the~\defemph{\Cstar-property}.
\end{definition}


\begin{theorem}[Gelfand--Naimark]
	Let~$A$ be a commutative~\Cstaralgebra.
	\begin{enumerate}
		\item
			There exists a locally compact Hausdorff space~$X$ such that~$A$ is isomorphic to~$\Cont_0(X)$.
		\item
			The space~$X$ is unique up to homeomorphism.
	\end{enumerate}
\end{theorem}


\begin{remark}
	The functor~$\Cont_0$ from the category of locally compact Hausdorff spaces to the category of commutative~\Cstaralgebras{} is an equivalence of categories.
\end{remark}


\begin{fluff}
	The theorom of Gelfand--Naimark leads to the idea behind \defemph{noncommutative geometry}.

	In a first step, we translate topological/geometric objects into the language of~\Cstaralgebras{}.
	This is to be done in such a way that one can reconstruct the original object from the occuring (commutative) operator algebras.

	In the second step we try to apply the constructions developed in the first step to noncommutative operator algebras.
\end{fluff}


\begin{fluff}
	There are basically two points of view on how to interpret a noncommutative operator algebra~$A$ in these situations:
	\begin{enumerate*}
		\item
			We can think of~$A$ as an algebra of fuctions on a \enquote{noncommutative space} (even though this space itself does not exist).
		\item
			We can think of~$A$ as describing a \enquote{badly behaved} (e.g. non-Hausdorff) topological space.
	\end{enumerate*}
	In this lecture course, we will mostly focus on the second point of view.
\end{fluff}





\section{Vector Bundles}

\begin{fluff}
	Let~$X$ be a compact Hausdorff space and let
	\[
		\pi \colon E \to X
	\]
	be a vector bundle over~$X$.
	The space of sections,
	\[
		\Sec(E)
		\defined
		\{
			\psi \colon X \to A
		\suchthat
			\text{$\psi$ is continuous with~$\pi \circ \psi = \id_X$}
		\} \,,
	\]
	can be endowed with the structure of a finitely generated, projective module over~$\Cont(X)$.
\end{fluff}


\begin{theorem}[Serre--Swan]
	Let~$X$ be a compact topological space and let~$M$ be a finitely generated, projective module over~$\Cont(X)$.
	There exists a vector bundle~$E$ over~$X$ such that~$M \cong \Sec(E)$.
\end{theorem}


\begin{remark}
	The theorem of Serre--Swan leads to an equivalence of categories between the category of vector bundles over~$X$ and the category of finitely generated, projective modules over~$\Cont(X)$.
\end{remark}


\begin{fluff}
	The above equivalence between vector bundles and modules leads to the following idea.
\end{fluff}


\begin{definition}
	\label{definition of noncommutative vector bundle in motivation}
	Let~$A$ be a~\Cstaralgebra.
	A finitely generated, projective module over~$A$ is a \defemph{noncommutative vector bundle} over~$A$.
\end{definition}


\begin{remark}
	Let~$A$ be a~\Cstaralgebra.
	There exist for every noncommutative vector bundle~$M$ over~$A$ a natural number~$k$ and a matrix~$p$ in~$\Mat(k, A)$ with~$p^2 = p$ and~$p = p^*$ such that~$M \cong p A^{\oplus k}$.
	We can therefore think about vector bundles over~$A$ as certain projection matrices.
\end{remark}





\section{K-Theory}

\begin{fluff}
	To study a (noncommutative) space, one can look for (topological) invariants of this space.
	For ordinary spaces, such invariants are in part provided by homology theories and cohomology theories (e.g. singular cohomology, Čhech cohomology, and de~Rham cohomology).
	For~\Cstaralgebras{}, a suitable kind of cohomology theory is given by~\defemph{\Ktheory}.
	
	For a compact Hausdorff space~$X$, the set of isomorphism classes of vector bundles over~$X$ forms an abelian monoid with respect to the direct sum of vector bundles.
	The~\defemph{topological \Ktheory} on~$X$ is the Grothendieck group of this abelian monoid.
	
	For a~\Cstaralgebra{}~$A$, its \defemph{topological~\Ktheory} will be similarly defined as the Grothendieck group of the abelian monoid of noncommutative vector bundles over~$A$ (in the sense of \cref{definition of noncommutative vector bundle in motivation}).
\end{fluff}





\section{K-Homology}

\begin{fluff}
	Dual to~\Ktheory, which is a cohomology theory, there ought to exist a homology theory, called \defemph{\Khomology}.
	It was found out by Michael Atiyah that elements of this~\Khomology{} can be explicitely expressed by elliptic operators.
	
	Consider an elliptic, selfadjoint, first-order differential operator~$D$ on a vector bundle~$F$ over a compact, smooth manifold~$M$.
	Let~$\Algebra$ be the algebra of smooth fuctions on~$M$, and let~$\Hilbert$ be the Hilbert space~$\Lp^2(F)$.
	Then the following holds:
	\begin{enumerate*}
		\item
			The commutator~$[D, a]$ is bounded for every~$a \in \Algebra$.
		\item
			The operator~$(1 + D^2)^{-1/2}$ is compact (because~$D$ is elliptic).
	\end{enumerate*}
	In general, a triple~$(\Algebra, \Hilbert, D)$ satisfying these two properties is a \defemph{spectral triple}.

	The~\Khomology{} of~$A$ will be constructed as certain equivalence classes of such spectral triples for which~$\Algebra$ is a dense subalgebra of~$A$.
\end{fluff}





\section{Index Pairing}

\begin{fluff}
	There is a pairing between~\Ktheory{} and~\Khomology{}, both of which are abelian groups:
	given a spectral triple~$(\Algebra, \Hilbert, D)$, there is an associated index map
	\[
		\Ind_D
		\colon
		\Kth(A)
		\to
		\Kth(\Complex)
		\cong
		\Integer \,,
		\quad
		\class{p}
		\mapsto
		\Ind(pDp) \,.
	\]
	(Here, one should thinks about the algebraic~\Ktheory{}~$\Kth(\Complex)$ as the topological~\Ktheory{} of the singleton space~$\{ \ast \}$.)
\end{fluff}

\begin{example}[Atiyah--Singer]
	On a Riemannien spin\textsuperscript{c} manifold~$M$ with spinor bundle~$S \to M$, we get a spectral triple given by
	\[
		( \Cont^\infty(M), \Lp^2(S), D ) \,,
	\]
	where~$D$ is the Dirac~operator coming from the Riemannian metric and the spin-structure of~$M$.
	Given a vector bundle~$E$ over~$M$, we have~$\Sec(E) \cong p \Cont(M)^{\oplus k}$ for some projection matrix~$p$ in~$\Mat(k, \Cont(M) )$.
	We can now realize the tensor product~$E \tensor S$ as a vector subbundle of the direct sum~$S^{\oplus k}$, and then consider the operator~$D_E \defined p D p$ on~$E \tensor S$.
	Then,~$\Ind_D(\class{p}) = \Ind(D_E)$.

	The Atiyah--Singer Index Theorem states that this index can be computed as the integral of a closed differential form.
\end{example}


\begin{fluff}
	One motivation behind noncommutative geometry is to provide the appropriate language to study index theory in greater generality (using noncommutative algebras).
	One may think, for example, about foliated manifolds, or manifolds together with a group action (in which case we would like to have an equivariant index theory).
\end{fluff}





\section{Examples: Bad quotients}


\begin{fluff}
	Let~$X$ be a compact Hausdorff space and consider an equivalence relation~$\sim$ on~$X$, determined by a subset~$R$ of~$X \times X$.
	We would like to study the quotient space~$Y \defined X / {\sim}$ in an operator-algebraic language.
	The algebra of continous functions~$\Cont(Y)$ can be rather bad at describing~$Y$:
	it may happen that~$\Cont(Y)$ consists only of constant functions, even if the space~$Y$ is very large.
	One solution to this problem comes from noncommutative geometry:
	we construct a noncommutative~\Cstaralgebra~$\Cont^*(Y)$ from~$(X, \sim)$ that sufficiently describes~$Y$.
\end{fluff}


\begin{example}
	\label{first example of matrices as functios space}
	We consider the two-point discrete space~$X = \{ x_1, x_2 \}$.
	The space~$X$ can be retrieved from its algebra of functions~$\Cont(X) \cong \Complex \times \Complex$.
	We consider now the equivalence relation~$\sim$ on~$X$ that identifies~$x_1$ and~$x_2$.
	The quotient space~$Y \defined X / {\sim}$ is actually a~\enquote{good quotient}:
	the space~$Y$ can be recovered from its space of functions~$\Cont(Y) \cong \Complex$.
	
	Let us nevertheless construct a noncommutative~\Cstaralgebra~$\Cont^*(Y)$ to describe~$Y$.
	The equivalence relation~$\sim$ corresponds to the subset~$R$ on~$X \times X$ given by
	\[
		R = \{ (x_1, x_1), (x_1, x_2), (x_2, x_1), (x_2, x_2) \} \,.
	\]
	We define~$\Cont^*(Y)$ as the vector space~$\Cont(R)$ together with the product
	\[
		(a b)(x, y)
		=
		\sum_{z \in X} a(x, z) b(z, y)
	\]
	and the antiinvolution
	\[
		a^*(x, y) = \conj{a(y, x)}
	\]
	for all~$a, b \in \Cont^*(Y)$ and~$x, y \in X$.

	To better understand~$\Cont^*(Y)$ let us rewrite the set~$X$ as~$X = \{1, 2\}$ and the set~$R$ as pairs of numbers
	\[
		R = \{ 11, 12, 21, 22 \} \,.
	\]
	We can then write every element~$a$ of~$\Cont^*(Y)$ in the form
	\[
		a
		=
		\begin{pmatrix}
			a_{11}  & a_{12} \\
			a_{21}  & a_{22}
		\end{pmatrix} \,,
	\]
	where~$a_{ij}$ it the entry of~$a$ on the pair~$ij$.
	We have now
	\[
		(a b)_{ij} = \sum_{k=1,2} a_{ik} b_{kj} \,,
		\quad
		(a^*)_{ij} = \conj{ a_{ji} } \,.
	\]
	We see from these formulas that~$\Cont^*(Y)$ is isomorphic to the matrix algebra~$\Mat(2, \Complex)$.
	
	We can now reconstruct~$Y$ as the set of irreducible representation of~$\Cont^*(Y)$.
	But we can also reconstruct the size of~$X$ as the dimension of the unique irreducible representation of~$\Cont^*(Y)$.
	The algebra~$\Cont^*(Y)$ does therefore remember more than just~$Y$ itself.
\end{example}


\begin{example}
	We consider now the topological space~$X = [0, 1] \amalg [0, 1]$ and the equivalence relation~$\sim$ on~$X$ that identifies for every element~$x$ in~$(0, 1)$ the copy of~$x$ in the first summand of~$X$ with the copy of~$x$ in the second summand of~$X$.
	We may think about the quotient~$Y \defined X / {\sim}$ as an interval with two endpoints on each side, see~\cref{line with two times two endpoints}.
	\begin{figure}
		\centering
		\begin{tikzcd}
			\draw (0,0) -- (5,0);
			\draw[fill] (0,  0.05) circle (0.04);
			\draw[fill] (0, -0.05) circle (0.04);
			\draw[fill] (5,  0.05) circle (0.04);
			\draw[fill] (5, -0.05) circle (0.04);
		\end{tikzcd}
		\caption{The quotient space~$Y$.}
		\label{line with two times two endpoints}
	\end{figure}
	We find that~$\Cont(Y)$ is the same as~$\Cont([0, 1])$, and therefore that~$Y$ cannot be reconstructed from~$\Cont(Y)$.
	
	Let~$R$ be the subset~$X \times X$ corresponding to the equivalence relation~$\sim$.
	We define the algebra~$\Cont^*(Y)$ as in~\cref{first example of matrices as functios space}, but find this time that~$\Cont^*(Y)$ is isomorphic to the subalgebra of~$\Cont( [0, 1], \Mat(2, \Complex) )$ given by
	\[
		\{
			f \in \Cont( [0, 1], \Mat(2, \Complex) )
		\suchthat
			\text{$f(0)$ and~$f(1)$ are diagonal matrices}
		\} \,.
	\]
	The irreducible representations of~$\Cont^*(Y)$ are parametrized by the elements of~$Y$.
	For~$x \in (0, 1)$ the corresponding representation is two-dimensional and given by
	\[
		\Cont^*(Y) \to \Mat(2, \Complex) \,,
		\quad
		f \mapsto f(x) \,,
	\]
	and for~$x \in \{0, 1\}$ and~$k = 1, 2$ the corresponding representation is one-dimensional and given by
	\[
		\Cont^*(Y) \to \Complex \,,
		\quad
		f \mapsto \text{$k$-th diagonal entry of~$f(x)$} \,.
	\]
	We can therefore recover~$Y$ from~$\Cont^*(Y)$.
\end{example}


\begin{example}
	Let~$X$ be the two-dimensional torus, realized as the quotient~$\Real^2 / \Integer^2$.
	We consider for an angle~$\theta$ with~$0 < \theta < 1$ the vector field~$V$ of~$X$ given by
	\[
		V = \partial_x + \theta \partial_y \,.
	\]
	We define an equivalence relation~$\sim$ on~$X$ such that two points~$p$ and~$q$ of~$X$ are equivalent if and only if they lie on the same integral curve of~$V$.
	Let~$Y$ be the quotient space~$X / {\sim }$.
	
	If~$\theta$ is rational then the integral curves of~$V$ are closed (loops), and~$Y$ is homeomorphic to the circle~$\Sphere^1$.
	If~$\theta$ is irrational, then each integral curve of~$V$ is dense in~$X$.
	It then follows that~$\Cont(Y)$ consists only of constant functions.
	For~$\Cont^*(Y)$ we can consider the algebra
	\[
		\Cont^*(Y)
		\cong
		A_\theta
		\tensor
		{\Kompact} \,,
	\]
	where~$A_\theta$ is the so-called irrational rotation algebra, and~$\Kompact$ is an algebra of compact operators.
	The algebra~$A_\theta$ may be described as~$\Cont^*(Y')$, where~$Y'$ is the quotient space of the circle~$\Real / \Integer$ by the equivalence relation~$\sim$ that is given by~$\class{s} \sim \class{t}$ if and only if~$s - t = k \theta$ for some integer~$k$.
\end{example}


\begin{remark}
	In the above example, we have~$\Kompact( \Cont^*(Y) ) \cong \Kompact( \Cont^*(Y') )$.
	(This it should be, since~$Y \cong Y'$).
\end{remark}





