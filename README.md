# Noncommutative Spaces, SS21

This repository contains my personal notes for the lecture course _Noncommutative Spaces_ that is given by Dr. Koen van den Dungen at the University of Bonn in the summer term of 2021.
A compiled version of these notes is available at [https://lecture-notes-bonn.gitlab.io/original/noncommutative-spaces-notes-ss21/nc-spaces-ss21.pdf][1].

[1]: https://lecture-notes-bonn.gitlab.io/original/noncommutative-spaces-notes-ss21/nc-spaces-ss21.pdf
